<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /** @var string */
    private $targetModel;

    /** @var array */
    private $headers;

    /** @var array */
    private $validationRules;

    /**
     * Controller constructor.
     *
     * @param string|null $targetModel
     */
    public function __construct($targetModel = null)
    {
        $this->targetModel = $targetModel ?? $this->resolveTargetModelFromController();
    }

    /**
     * @return string
     */
    private function resolveTargetModelFromController(): string
    {
        $currentModel = substr(static::class, 0, -10);
        $currentModel = substr(strrchr($currentModel, "\\"), 1);

        return '\\NorthernLights\\ExampleApp\\Model\\' . $currentModel;
    }

    /**
     * Setup validation rules
     *
     * @param array $rules
     *
     * @return Controller
     */
    protected function setValidationRules(array $rules): self
    {
        $this->validationRules = $rules;

        return $this;
    }

    /**
     * Get validation rules
     *
     * @return array
     */
    protected function getValidationRules():? array
    {
        return $this->validationRules;
    }

    /**
     * Validate data
     *
     * @param Request $request
     * @param bool $force
     *
     * @return Request
     *
     * @throws ValidationException
     */
    protected function validateData(Request $request, $force = false): Request
    {
        // Skip validation if there are no rules
        if ($force || $this->hasValidationRules()) {
            $this->validate($request, $this->validationRules);
        }

        return $request;
    }

    /**
     * Tell whether or not there are validation rules
     *
     * @return bool
     */
    protected function hasValidationRules(): bool
    {
        return $this->validationRules !== null;
    }
}
